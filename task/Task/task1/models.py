from django.db import models
import datetime


# Create your models here.

class Department(models.Model):
    DeptID = models.IntegerField(null=False)
    DeptName = models.CharField(max_length=122, null=False)
    DeptCode = models.CharField(max_length=122, null=True)
    ParentDeptID = models.IntegerField(null=False)
    ManagerID = models.IntegerField(null=True)
    Description = models.TextField(null=True)
    Active = models.BooleanField(null=False)

    def __str__(self):
        return self.DeptID


class Employee(models.Model):
    EmployeeID = models.IntegerField(null=False)
    FirstName = models.CharField(max_length=122, null=False)
    MiddleName = models.CharField(max_length=122, null=True)
    LastName = models.CharField(max_length=122, null=False)
    JoinDate = models.DateTimeField(null=True)
    MonthlySalary = models.DecimalField(null=True)
    DeptID = models.ForeignKey(Department, on_delete=models.CASCADE)

    def __str__(self):
        return self.EmployeeID

    @property
    def WorkedMonths(self):
        CurrentDate = datetime.datetime.now()
        TotalMonths = CurrentDate - self.JoinDate
        return TotalMonths

    @property
    def TotalSalary(self):
        return self.TotalMonths * self.MonthlySalary

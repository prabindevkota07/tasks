from django.contrib import admin
from . import models
# Register your models here.
admin.site.register(Department)
admin.site.register(Employee)